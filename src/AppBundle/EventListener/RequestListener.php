<?php
namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Repository\RepositoryFactory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class RequestListener
{
    public function __construct(EntityManager $em, TokenStorage $token) {
        $this->token = $token;
        $this->em = $em;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $token = $this->token->getToken();
        $request = $event->getRequest();
        $_route  = $request->attributes->get('_route');

        if ( strpos($_route, '_') === 0 ) return null;

        if (!$token) return null;

        if ($user = $token->getUser()) {
            if ( !is_object($user) ) {
                // e.g. anonymous authentication
                return null;
            }
        }

        $user->setPage($_route);
        $this->em->persist($user);
        $this->em->flush($user);
    }
}