<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'main-menu list-group');

        $menu->addChild('Home', array('route' => 'homepage'));
        $menu->addChild('Tasks', array('route' => 'taskspage'));
        $menu->addChild('Results', array('route' => 'resultspage'));
        $menu->addChild('About', array('route' => 'aboutpage'));

        foreach ( $menu as $key => $item ) {
            $item->setAttribute('class', 'list-group-item');
        }

        return $menu;
    }
    
    public function navBar(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('navbar');
        $menu->setChildrenAttribute('class', 'nav navbar-nav pull-right');

        $sc = $this->container->get('security.context');

        if ( $sc->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {
            $menu->addChild('Logout', array('route' => 'logout'));
        }
        else {
            $menu->addChild('Login', array('route' => 'login'));
            $menu->addChild('Registration', array('route' => 'registration'));
        }

        foreach ( $menu as $key => $item ) {
            $item->setAttribute('class', 'nav-item');
        }

        return $menu;
    }
}