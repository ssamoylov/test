<?php

namespace AppBundle\Model;

use Symfony\Component\DomCrawler\Crawler;

class Parser
{
    public function getPosts($url)
    {
        $pages = [
            file_get_contents($url),
            file_get_contents($url . '/page/2'),
            file_get_contents($url . '/page/3'),
        ];

        $posts = [];

        foreach ( $pages as $html ) {
            $posts = array_merge($posts, $this->_getPostsFromHtml($html));
        }

        return $posts;
    }

    protected function _getPostsFromHtml($html)
    {
        $crawler = new Crawler($html);
        $posts = $crawler->filterXPath('//article[contains(@id, "post-")]')->each(function (Crawler $node, $i) {
            $url = $node->filter('h1 a')->attr('href');
            $image = '';

            if ( !empty($node->filter('div.ytp-thumbnail-overlay-image')->count()) ) {
                $image = $node->filter('div.ytp-thumbnail-overlay-image')->attr('style');
                $image = preg_match('/url\("([^"]+)"\)/', $image);
            }

            if ( !empty($node->filter('div.entry-content img')->count()) ) {
                $image = $node->filter('div.entry-content img')->attr('src');
            }

            $full_post_html = file_get_contents($url);
            $post_crawler = new Crawler($full_post_html);

            $post = [
                'url' => trim($url),
                'title' => trim($node->filter('h1')->text()),
                'author' => trim($post_crawler->filter('h2 > span.fn')->text()),
                'date' => date_create(date('Y-m-d', strtotime($node->filter('time')->attr('datetime')))),
                'image' => $image,
                'body' => trim($post_crawler->filterXPath('//div[@class="entry-content"]')->html()),
            ];

            $post_crawler->clear();
            return $post;
        });
        $crawler->clear();
        return $posts;
    }
}