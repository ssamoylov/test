<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Parser;
use AppBundle\Entity\Post;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\BrowserKit\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/tasks", name="taskspage")
     */
    public function tasksAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('parse', 'submit', ['label' => 'Parse', 'attr' => ['class' => 'btn btn-success']])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $parser = new Parser();
            $posts = $parser->getPosts('http://wwwhatsnew.com');

            if (!empty($posts)) {

                $em = $this->getDoctrine()->getManager();

                foreach ($posts as $post_data) {
                    $post = $this->getDoctrine()
                        ->getRepository('AppBundle:Post')
                        ->findOneBy(['url' => $post_data['url'], 'title' => $post_data['title']]);

                    if (!empty($post)) continue;

                    $post = new Post();
                    $post->setUrl($post_data['url']);
                    $post->setTitle($post_data['title']);
                    $post->setAuthor($post_data['author']);
                    $post->setDate($post_data['date']);
                    $post->setImage($post_data['image']);
                    $post->setBody($post_data['body']);

                    $em->persist($post);
                    $em->flush();
                }

                $this->addFlash('notice', 'Loading data was completed!');
            }
        }

        // replace this example code with whatever you need
        return $this->render('default/tasks.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/results", name="resultspage")
     */
    public function resultsAction(Request $request)
    {
        $filter = $this->createFormBuilder(null, ['attr' => ['id' => 'post-filter', 'action' => '/']])
            ->add('daterange', 'text', ['label' => 'Choose the period', 'attr' => ['value' => false]])
            ->add('show', 'submit', ['label' => 'Show', 'attr' => ['class' => 'btn-primary']])
            ->getForm();
        $filter->handleRequest($request);

        $posts = $this->_getPosts();

        if ($filter->isSubmitted()) {
            $_post = $filter->getData();

            if (!empty($_post['daterange'])) {
                $posts = $this->_getPosts($_post['daterange']);

                if (empty($posts)) {
                    $this->addFlash('warning', 'Posts for this period are not found!');
                }
            }
        }

        arsort($posts);
        $fusionData = $this->_getChartsData($posts);
        sort($posts);

        // replace this example code with whatever you need
        return $this->render('default/results.html.twig', [
            'posts' => $posts,
            'fusionData' => $fusionData,
            'filter' => $filter->createView()
        ]);
    }

    /**
     * @Route("/about", name="aboutpage")
     */
    public function aboutAction(Request $request)
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/filter-posts", name="filter_posts")
     */
    public function ajaxFilterPostsAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) return null;

        $_dateRange = $request->request->get('daterange');
        $posts = $this->_getPosts($_dateRange);

        arsort($posts);
        $fusionData = $this->_getChartsData($posts);
        sort($posts);
        $posts_content = $this->renderView('default/posts/table.html.twig', ['posts' => $posts]);

        $response = [
            'success' => true,
            'posts' => "$posts_content",
            'fusionData' => $fusionData
        ];

        if (empty($posts)) {
            $response = [
                'error' => true,
                'message' => 'Posts for this period are not found!'
            ];
        }

        return new JsonResponse($response);
    }

    protected function _getPosts($dateRange = null)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Post');
        $posts = $repository->findAll();

        if (!empty($dateRange)) {
            $dateRange = explode(' - ', $dateRange);
            $posts = $repository->createQueryBuilder('p')
                ->where('p.date BETWEEN :min_date AND :max_date')
                ->setParameter('min_date', date_create($dateRange[0]))
                ->setParameter('max_date', date_create($dateRange[1]))
                ->orderBy("p.date", "ASC")
                ->getQuery()
                ->getResult();
        }

        return $posts;
    }

    protected function _getChartsData(array $posts)
    {
        $fusionData = [];

        foreach ($posts as $post) {
            $key = date_format($post->getDate(), 'Ymd');
            $fusionData[$key]["label"] = date_format($post->getDate(), 'Y-m-d');
            $fusionData[$key]["value"] = !empty($fusionData[$key]["value"]) ? $fusionData[$key]["value"] + 1 : 1;
        }

        return array_values($fusionData);
    }
}
