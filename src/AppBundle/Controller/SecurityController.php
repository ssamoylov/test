<?php
namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/login.html.twig', [
            'error' => $error,
        ]);
    }

    /**
     * @Route("/register", name="registration")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $authKey = md5(random_int(1,100) + time());
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());

            $user->setPassword($password);
            $user->setAuthKey($authKey);
            $user->setIsActive(false);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->flush();

            $message_body = $this->renderView('Emails/register.html.twig', [
                'username' => $user->getUsername(),
                'authKey' => $authKey
            ]);

            $message = \Swift_Message::newInstance()
                ->setSubject('Activation account from Test')
                ->setFrom('admin@test.my')
                ->setTo($user->getEmail())
                ->setBody($message_body, 'text/html');

            $mailer = $this->get('mailer');
            $mailer->send($message);

            if ( $this->get('mailer')->send($message) ) {
                $this->addFlash('notice', 'Activate your account from email');
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/activate/{authKey}", name="activationpage")
     */
    public function activateAction(Request $request, $authKey)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $repository->findOneBy(['authKey' => $authKey]);

        if (empty($user)) {
            $this->addFlash('error', 'Account with key "' . $authKey . '" does not exists');
            return $this->redirectToRoute('registration');
        }

        $user->setIsActive(true);
        $manager = $this->getDoctrine()->getManager();
        $manager->flush();
        $this->addFlash('notice', 'You account has been activated');

        return $this->redirectToRoute('homepage');
    }

}